package com.zhr.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.zhr.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhr
 */
@Component
public interface UserMapper extends BaseMapper<User> {

    /**
     * 自定义 SQL
     *
     * @param wrapper
     * @return
     */
    List<User> selectAll(@Param(Constants.WRAPPER) Wrapper<User> wrapper);

    @Select("select * from user ${ew.customSqlSegment}")
    List<User> selectAllInJava(@Param(Constants.WRAPPER) Wrapper<User> wrapper);
}
