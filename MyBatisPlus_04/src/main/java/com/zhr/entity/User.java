package com.zhr.entity;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author zhr
 */
@Data
@Accessors(chain = true)
public class User {
    /**
     * 主键
     */
    private Long id;
    /**
     * 姓名
     */
    @TableField(condition = SqlCondition.LIKE)
    private String name;
    /**
     * 年龄
     */
    @TableField(condition = "%s&lt;#{%s}")
    private Integer age;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 直属上级
     */
    private Long managerId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
