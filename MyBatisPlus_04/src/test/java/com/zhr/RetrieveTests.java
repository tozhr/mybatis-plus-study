package com.zhr;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.zhr.dao.UserMapper;
import com.zhr.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class RetrieveTests {
    @Autowired
    private UserMapper userMapper;

    @Test
    void selectById() {
        User user = userMapper.selectById(1094590409767661570L);
        System.out.println(user);
    }

    @Test
    void selectByIds() {
        List<User> users = userMapper.selectBatchIds(Arrays.asList(1094590409767661570L, 1094592041087729666L));
        users.forEach(System.out::println);
    }

    @Test
    void selectByMap() {
        Map<String, Object> map = new HashMap<>();
        //key 数据库中的列 不是实体类中的字段
        map.put("name", "王天风");
        map.put("age", 25);
        List<User> users = userMapper.selectByMap(map);
        users.forEach(System.out::println);
    }

    /**
     * 1、名字中包含雨并且年龄小于40
     * name like '%雨%' and age<40
     */
    @Test
    void selectByWrapper1() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //QueryWrapper<User> wrapper = Wrappers.<User>query();
        wrapper.like("name", "雨").lt("age", "40");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 2、名字中包含雨年并且龄大于等于20且小于等于40并且email不为空
     * name like '%雨%' and age between 20 and 40 and email is not null
     */
    @Test
    void selectByWrapper2() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //直接调用 and 连接
        wrapper.like("name", "雨").between("age", 20, 40).isNotNull("email");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 3 名字为王姓或者年龄大于等于25，按照年龄降序排列，年龄相同按照id升序排列
     * name like '王%' or age>=25 order by age desc,id asc
     */
    @Test
    void selectByWrapper3() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //直接调用 and 连接
        wrapper.likeRight("name", "王").or().ge("age", 25)
                .orderByDesc("age").orderByAsc("id");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 4、创建日期为2019年2月14日并且直属上级为名字为王姓
     * date_format(create_time,'%Y-%m-%d')='2019-02-14' and manager_id in (select id from user where name like '王%')
     */
    @Test
    void selectByWrapper4() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //直接调用 and 连接
        wrapper.apply("date_format(create_time,'%Y-%m-%d')={0}", "2019-02-14")
                .inSql("manager_id", "select id from user where name like '王%'");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 5、名字为王姓并且（年龄小于40或邮箱不为空）
     * name like '王%' and (age<40 or email is not null)
     */
    @Test
    void selectByWrapper5() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //直接调用 and 连接
        wrapper.likeRight("name", "王").and(wq -> wq.lt("age", 40).or().isNotNull("email"));
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 名字为王姓或者（年龄小于40并且年龄大于20并且邮箱不为空）
     * name like '王%' or (age<40 and age>20 and email is not null)
     */
    @Test
    void selectByWrapper6() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //直接调用 and 连接
        wrapper.likeRight("name", "王")
                .or(wq -> wq.lt("age", 40).gt("age", 20).isNotNull("email"));
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 7、（年龄小于40或邮箱不为空）并且名字为王姓
     * (age<40 or email is not null) and name like '王%'
     */
    @Test
    void selectByWrapper7() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        //直接调用 and 连接
        wrapper.nested(wq -> wq.lt("age", 40).isNotNull("email"))
                .likeRight("name", "王");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 8、年龄为30、31、34、35
     * age in (30、31、34、35)
     */
    @Test
    void selectByWrapper8() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.in("age", Arrays.asList(30, 31, 34, 35));
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 9、只返回满足条件的其中一条语句即可
     * limit 1
     */
    @Test
    void selectByWrapper9() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.in("age", Arrays.asList(30, 31, 34, 35)).last("limit 1");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 10、名字中包含雨并且年龄小于40(需求1加强版)
     * 第一种情况：select id,name
     * from user
     * where name like '%雨%' and age<40
     * 第二种情况：select id,name,age,email
     * from user
     * where name like '%雨%' and age<40
     */
    @Test
    void selectByWrapper10_1() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.select("id", "name").like("name", "雨").lt("age", "40");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    @Test
    void selectByWrapper10_2() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.select(User.class, tableFieldInfo -> !tableFieldInfo.getColumn().equals("create_time") && !tableFieldInfo.getColumn().equals("manager_id"))
                .like("name", "雨").lt("age", "40");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    @Test
    void selectByWrapperCondition() {
        String name = "";
        String age = "";
        testCondition(name, age);

    }

    private void testCondition(String name, String age) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like(StringUtils.isNotBlank(name), "name", name)
                .like(StringUtils.isNotBlank(age), "age", age);
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    @Test
    void selectByWrapperByEntity() {
        User user = new User().setAge(40);
        //实体中有值字段变为查询条件
        //！！！ 重要 且和 wrapper中的SQL不冲突 即同时出现
        //默认 =值
        // 在实体上增加注解可以自定义
        QueryWrapper<User> wrapper = new QueryWrapper<>(user);
        wrapper.like("name", "雨").lt("age", "40");
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    @Test
    void selectByWrapperAllEq() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        Map<String, Object> params = new HashMap<>();
        params.put("name", "王天风");
        params.put("age", 40);
        //params.put("age", null);
        //wrapper.allEq(params);
        //null 的字段忽略
        //wrapper.allEq(params, false);
        //字段过滤
        wrapper.allEq((k, v) -> !k.equals("name"), params, false);
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);
    }

    @Test
    void selectByWrapperMaps_1() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.select("id", "name").like("name", "雨").lt("age", "40");
        List<Map<String, Object>> users = userMapper.selectMaps(wrapper);
        users.forEach(System.out::println);
    }

    /**
     * 11、按照直属上级分组，查询每组的平均年龄、最大年龄、最小年龄。
     * 并且只取年龄总和小于500的组。
     * select avg(age) avg_age,min(age) min_age,max(age) max_age
     * from user
     * group by manager_id
     * having sum(age) <500
     */
    @Test
    void selectByWrapperMaps_2() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.select("manager_id", "avg(age) avg_age", "min(age) min_age", "max(age) max_age")
                .groupBy("manager_id")
                .having("sum(age)<{0}", 500);
        List<Map<String, Object>> users = userMapper.selectMaps(wrapper);
        users.forEach(System.out::println);
    }

    @Test
    void selectByWrapperObjs() {
        //查询多列 返回第一列
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.select("id", "name").like("name", "雨").lt("age", "40");
        List<Object> users = userMapper.selectObjs(wrapper);
        users.forEach(System.out::println);
    }

    @Test
    void selectByWrapperCount() {
        //查询多列 返回第一列
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("name", "雨").lt("age", "40");
        Integer users = userMapper.selectCount(wrapper);
        System.out.println(users);
    }

    @Test
    void selectByWrapperSelectOne() {
        //查询1或0列 返回多于1列会报错
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("name", "雨").lt("age", "40");
        User users = userMapper.selectOne(wrapper);
        System.out.println(users);
    }


    @Test
    void selectLambda_1() {
        //LambdaQueryWrapper<User> wrapper = new QueryWrapper<User>().lambda();
        //LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
        wrapper.like(User::getName, "雨").lt(User::getAge, 40);
        User users = userMapper.selectOne(wrapper);
        System.out.println(users);
    }

    /**
     * 5、名字为王姓并且（年龄小于40或邮箱不为空）
     * name like '王%' and (age<40 or email is not null)
     */
    @Test
    void selectLambda_2() {
        LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
        wrapper.likeRight(User::getName, "王")
                .and(lw -> lw.lt(User::getAge, 40).or().isNotNull(User::getEmail));
        User users = userMapper.selectOne(wrapper);
        System.out.println(users);
    }

    @Test
    void selectLambda_3() {
        List<User> users = new LambdaQueryChainWrapper<>(userMapper)
                .like(User::getName, "王").list();
        System.out.println(users);
    }

    @Test
    void selectMyInJava() {
        LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
        wrapper.likeRight(User::getName, "王")
                .and(lw -> lw.lt(User::getAge, 40).or().isNotNull(User::getEmail));
        List<User> users = userMapper.selectAllInJava(wrapper);
        System.out.println(users);
    }

    @Test
    void selectMyInXml() {
        LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
        wrapper.likeRight(User::getName, "王")
                .and(lw -> lw.lt(User::getAge, 40).or().isNotNull(User::getEmail));
        List<User> users = userMapper.selectAll(wrapper);
        System.out.println(users);
    }
}
























