package com.zhr.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhr.entity.User;

/**
 * @author zhr
 */
public interface UserMapper extends BaseMapper<User> {


}
