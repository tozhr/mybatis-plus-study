package com.zhr.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author zhr
 * TableName    表名 不写默认_转驼峰
 * TableId      主键 不写默认_转驼峰
 * TableField   列名 不写默认_转驼峰
 */
@Data
@Accessors(chain = true)
@TableName("user")
public class User {
    /**
     * 主键
     */
    @TableId("id")
    private Long id;
    /**
     * 姓名
     */
    @TableField("name")
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 直属上级
     */
    private Long managerId;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 数据库中没有的字段
     * 1 加关键字  transient 不参与序列化
     * 2 加关键字  static
     * 3 注解      TableField(exist = false)
     */
    @TableField(exist = false)
    private  String remark;

}
