package com.zhr.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhr.entity.Department;
import org.springframework.stereotype.Component;

/**
 * @author zhr
 */
@Component
public interface DepartmentMapper extends BaseMapper<Department> {
}
