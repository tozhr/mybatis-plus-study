package com.zhr.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhr.entity.User;

public interface IUserService extends IService<User> {
}
