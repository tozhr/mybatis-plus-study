package com.zhr.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhr.dao.UserMapper;
import com.zhr.entity.User;
import com.zhr.service.IUserService;
import org.springframework.stereotype.Service;

/**
 * @author zhr
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
}
