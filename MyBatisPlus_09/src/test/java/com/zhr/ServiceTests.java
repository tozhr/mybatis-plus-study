package com.zhr;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhr.entity.User;
import com.zhr.service.IUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ServiceTests {
    @Autowired
    private IUserService iUserService;

    @Test
    public void testInsert() {
        List<User> list = new ArrayList<>();
        boolean b = iUserService.saveBatch(list, 100);
    }

    @Test
    public void testget() {
        Page<User> page = iUserService.lambdaQuery().eq(User::getAge, 34)
                .page(new Page<>(1, 2, false));
    }

    @Test
    public void remove() {
        boolean remove = iUserService.lambdaUpdate().eq(User::getAge, 34).remove();
    }
}
























