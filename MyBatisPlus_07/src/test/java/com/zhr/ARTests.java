package com.zhr;

import com.zhr.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
class ARTests {
    @Test
    public void testInsert() {
        boolean b = new User()
                .setName("刘明强")
                .setAge(31)
                .setManagerId(1088248166370832385L)
                .setCreateTime(LocalDateTime.now())
                .insert();
        System.out.println("b = " + b);
    }

    @Test
    void selectById_01() {
        User user = new User();
        User selectById = user.selectById(1088248166370832385L);
        System.out.println("selectById = " + selectById);
        System.out.println("(selectById==user) = " + (selectById == user));
    }

    @Test
    void selectById_02() {
        User user = new User();
        user.setId(1088248166370832385L);
        User selectById = user.selectById();
        System.out.println("selectById = " + selectById);
        System.out.println("(selectById==user) = " + (selectById == user));
    }

    @Test
    void updateById() {
        User user = new User();
        user.setId(1088248166370832385L);
        user.setName("王大锤");
        boolean update = user.updateById();
        System.out.println("update = " + update);
    }

    @Test
    void deleteById() {
        User user = new User();
        user.setId(108824816670832385L);
        boolean deleteById = user.deleteById();
        System.out.println("deleteById = " + deleteById);
    }

    @Test
    void insertOrUpdate() {
        User user = new User();
        user.setId(108824816670832385L).setName("lalala");
        boolean insertOrUpdate = user.insertOrUpdate();
        System.out.println("insertOrUpdate = " + insertOrUpdate);
    }
}
























