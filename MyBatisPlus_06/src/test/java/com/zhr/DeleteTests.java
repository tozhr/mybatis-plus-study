package com.zhr;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.zhr.dao.UserMapper;
import com.zhr.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class DeleteTests {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void testDeleteById() {
        int i = userMapper.deleteById(10882481637083238L);
        System.out.println("影响记录数量：" + i);
    }

    @Test
    public void testDeleteByMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("age", 55);
        map.put("name", "lal");
        int i = userMapper.deleteByMap(map);
        System.out.println("影响记录数量：" + i);
    }

    @Test
    public void testDeleteByIds() {
        int i = userMapper.deleteBatchIds(Arrays.asList(1, 2, 3));
        System.out.println("影响记录数量：" + i);
    }

    @Test
    public void testDeleteByWrapper() {
        LambdaQueryWrapper<User> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(User::getAge, 55);
        int i = userMapper.delete(wrapper);
        System.out.println("影响记录数量：" + i);
    }
}
























