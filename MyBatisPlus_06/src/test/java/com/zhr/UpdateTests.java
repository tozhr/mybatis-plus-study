package com.zhr;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.zhr.dao.UserMapper;
import com.zhr.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UpdateTests {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void testUpdateById() {
        User user = new User().setId(1088248166370832385L).setAge(27);
        int i = userMapper.updateById(user);
        System.out.println("影响记录数量：" + i);
    }


    @Test
    public void testUpdateByWrapper_01() {
        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
        wrapper.eq("age", 25);
        User user = new User().setAge(27);
        int i = userMapper.update(user, wrapper);
        System.out.println("影响记录数量：" + i);
    }

    @Test
    public void testUpdateByWrapper_02() {
        User whereUser = new User().setAge(27);
        whereUser.setAge(27);
        //构造函数和SQL生成方法互不干扰同时生效
        UpdateWrapper<User> wrapper = new UpdateWrapper<>(whereUser);
        wrapper.eq("age", 25);
        User user = new User().setAge(27);
        int i = userMapper.update(user, wrapper);
        System.out.println("影响记录数量：" + i);
    }

    @Test
    public void testUpdateByWrapper_03() {
        UpdateWrapper<User> wrapper = new UpdateWrapper<>();
        wrapper.eq("age", 25).set("age", 24);
        int i = userMapper.update(null, wrapper);
        System.out.println("影响记录数量：" + i);
    }


    @Test
    public void testUpdateByWrapperLambda() {
        LambdaUpdateWrapper<User> wrapper = Wrappers.lambdaUpdate();
        wrapper.eq(User::getAge, 25).set(User::getAge, 24);
        int i = userMapper.update(null, wrapper);
        System.out.println("影响记录数量：" + i);
    }

    @Test
    public void testUpdateByWrapperLambdaChain() {
        boolean update = new LambdaUpdateChainWrapper<>(userMapper)
                .eq(User::getAge, 25).set(User::getAge, 24).update();
        System.out.println("state：" + update);
    }
}
























