package com.zhr.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserInfo {
    private Long userId;
    private String nickName;
    private String headIcon;
    private String motto;
}
