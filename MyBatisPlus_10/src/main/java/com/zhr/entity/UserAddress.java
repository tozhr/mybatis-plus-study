package com.zhr.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserAddress {
  private Long id;
  private Long userId;
  private String address;
  private Integer isDefault;
}
