package com.zhr;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhr.dao.UserVoMapper;
import com.zhr.entity.UserVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class RetrieveTests {

    @Autowired
    private UserVoMapper voMapper;

    /**
     * 自定义分页
     */
    @Test
    void selectByMyPage1() {
        QueryWrapper<UserVo> wrapper = new QueryWrapper<>();
        wrapper.like("name", "雨");
        //一条SQL
        Page<UserVo> page = new Page<>(1, 2);
        IPage<UserVo> userPage = voMapper.selectUserPage(page, wrapper);
        System.out.println(userPage.getPages());
        System.out.println(userPage.getTotal());
        List<UserVo> users = userPage.getRecords();
        users.forEach(System.out::println);
    }
}
























