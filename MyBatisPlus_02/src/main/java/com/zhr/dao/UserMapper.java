package com.zhr.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhr.entity.User;
import org.springframework.stereotype.Component;

@Component
public interface UserMapper extends BaseMapper<User> {
}
