package com.zhr;

import com.zhr.dao.UserMapper;
import com.zhr.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @RunWith(SpringRunner.class) 在Spring环境下运行Junit测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class MybatisplusApplicationTests {
    @Autowired
    private UserMapper userMapper;

    @Test
    void insert() {
        //没有Email字段 则生成的SQL中也不会生成该列
        User user = new User();
        user.setName("刘明强").setAge(31).setManagerId(1088248166370832385L)
                .setCreateTime(LocalDateTime.now());
        int insert = userMapper.insert(user);
        System.out.println("影响记录数 = " + insert);
    }

}
