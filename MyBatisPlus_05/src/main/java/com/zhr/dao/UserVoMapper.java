package com.zhr.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhr.entity.UserVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * @author zhr
 */
@Component
public interface UserVoMapper extends BaseMapper<UserVo> {

    IPage<UserVo> selectUserPage(Page<UserVo> page, @Param(Constants.WRAPPER) Wrapper<UserVo> wrapper);
}
