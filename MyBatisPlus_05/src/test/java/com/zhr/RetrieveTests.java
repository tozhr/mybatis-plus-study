package com.zhr;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhr.dao.UserMapper;
import com.zhr.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;

@SpringBootTest
class RetrieveTests {
    @Autowired
    private UserMapper userMapper;

    /**
     * 1、名字中包含雨并且年龄小于40
     * name like '%雨%' and age<40
     */
    @Test
    void selectByPage() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("name", "雨");
        //2条SQL
        Page<User> page = new Page<>(1, 2);
        IPage<User> userPage = userMapper.selectPage(page, wrapper);
        System.out.println(userPage.getPages());
        System.out.println(userPage.getTotal());
        List<User> users = userPage.getRecords();
        users.forEach(System.out::println);
    }

    @Test
    void selectByPageMap() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("name", "雨");
        IPage<Map<String, Object>> page = new Page<>(1, 2);
        IPage<Map<String, Object>> userPage = userMapper.selectMapsPage(page, wrapper);
        System.out.println(userPage.getPages());
        System.out.println(userPage.getTotal());
        List<Map<String, Object>> users = userPage.getRecords();
        users.forEach(System.out::println);
    }

    @Test
    void selectByPage_2() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("name", "雨");
        //一条SQL
        Page<User> page = new Page<>(1, 2,false);
        IPage<User> userPage = userMapper.selectPage(page, wrapper);
        System.out.println(userPage.getPages());
        System.out.println(userPage.getTotal());
        List<User> users = userPage.getRecords();
        users.forEach(System.out::println);
    }
    /**
     * 自定义分页
     */
    @Test
    void selectByMyPage() {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.like("name", "雨");
        //一条SQL
        Page<User> page = new Page<>(1, 2);
        IPage<User> userPage = userMapper.selectUserPage(page, wrapper);
        System.out.println(userPage.getPages());
        System.out.println(userPage.getTotal());
        List<User> users = userPage.getRecords();
        users.forEach(System.out::println);
    }
}
























